import React, { useEffect, useState } from "react";
import {
  Container,
  SubContainer,
  Image,
  Title,
  PhotosContainer,
  PhotosWrapper,
  LoadMoreButton,
} from "./styled";
import axios from "axios";
import Skeleton from "@mui/material/Skeleton";
import { useDispatch, useSelector } from "react-redux";
import { boardBackgroundUpdate } from "../../../../Services/boardService";
import LoadingScreen from "../../../LoadingScreen"; // Import the LoadingScreen component

import pLimit from "p-limit";
const limit = pLimit(5);
const getImages = async () => {
  const urls = Array.from(
    { length: 10 },
    (_, i) => `https://source.unsplash.com/random/150?sig=${i}`
  );
  const promises = urls.map((url) => limit(() => axios.get(url)));
  const imageResponses = await Promise.all(promises);
  return imageResponses.map((response, index) => ({
    id: index,
    urls: {
      small: response.request.responseURL,
      full: response.request.responseURL,
    },
  }));
};

const DefaultMenu = (props) => {
  return (
    <Container>
      <SubContainer onClick={() => props.menuCallback("Photos by Unsplash")}>
        <Image link="https://a.trellocdn.com/prgb/dist/images/photos-thumbnail@3x.8f9c1323c9c16601a9a4.jpg" />
        <Title>Photos</Title>
      </SubContainer>
      <SubContainer onClick={() => props.menuCallback("Colors")}>
        <Image link="https://a.trellocdn.com/prgb/dist/images/colors@2x.ec32a2ed8dd8198b8ef0.jpg" />
        <Title>Colors</Title>
      </SubContainer>
    </Container>
  );
};

const PhotosMenu = (props) => {
  const [images, setImages] = useState([]);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    loadImages();
  }, [page]);

  const loadImages = () => {
    setIsLoading(true);
    getImages(page).then((res) => {
      setImages((prevImages) => [...prevImages, ...res]);
      setIsLoading(false);
    });
  };

  const handleLoadMore = () => {
    setPage((prevPage) => prevPage + 1);
  };

  const handleClick = (background) => {
    boardBackgroundUpdate(props.boardId, background, true, props.dispatch);
  };

  return (
    <PhotosContainer>
      {images.length > 0
        ? images.map((image) => {
            return (
              <PhotosWrapper
                key={image.id}
                onClick={() => handleClick(image.urls.full)}
              >
                <Image key={image.id} link={image.urls.small} />
              </PhotosWrapper>
            );
          })
        : [...Array(18).keys()].map((l, i) => (
            <PhotosWrapper key={i}>
              <Skeleton variant="rectangular" width="100%" height="6rem" />
            </PhotosWrapper>
          ))}
      {isLoading ? (
        <LoadingScreen />
      ) : (
        <LoadMoreButton onClick={handleLoadMore}>Load More</LoadMoreButton>
      )}
    </PhotosContainer>
  );
};

const ColorsMenu = (props) => {
  const colors = [
    "#0079bf",
    "#d29034",
    "#519839",
    "#b04632",
    "#89609e",
    "#cd5a91",
    "#4bbf6b",
    "#00aecc",
  ];

  const handleClick = (background) => {
    boardBackgroundUpdate(props.boardId, background, false, props.dispatch);
  };

  return (
    <PhotosContainer>
      {colors.map((image) => {
        return (
          <PhotosWrapper key={image} onClick={() => handleClick(image)}>
            <Image key={image} bg={image} />
          </PhotosWrapper>
        );
      })}
    </PhotosContainer>
  );
};

const BackgroundMenu = (props) => {
  const dispatch = useDispatch();
  const boardId = useSelector((state) => state.board.id);
  return (
    <>
      {props.sectionName === "Change background" ? (
        <DefaultMenu {...props} dispatch={dispatch} boardId={boardId} />
      ) : props.sectionName === "Photos by Unsplash" ? (
        <PhotosMenu {...props} dispatch={dispatch} boardId={boardId} />
      ) : (
        <ColorsMenu {...props} dispatch={dispatch} boardId={boardId} />
      )}
    </>
  );
};

export default BackgroundMenu;
