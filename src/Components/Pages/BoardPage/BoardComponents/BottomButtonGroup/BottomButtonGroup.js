import React from 'react';
import CancelIcon from '@mui/icons-material/Cancel';
import * as style from './styled';

const BottomButtonGroup = (props) => {
	const { clickCallback, closeCallback, title } = props;
	return (
		<style.Row>
			<style.AddListButton onClick={() => clickCallback()}>{title}</style.AddListButton>
			<style.IconWrapper>
			<CancelIcon onClick={() => closeCallback()} fontSize='medium'/>
			</style.IconWrapper>
		</style.Row>
	);
};

export default BottomButtonGroup;
