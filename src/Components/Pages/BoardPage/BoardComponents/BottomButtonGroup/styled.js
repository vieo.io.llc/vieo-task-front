import styled from "styled-components";
import { InviteButton } from "../TopBar/styled";
export const Row = styled.div `
display:flex;
flex-direction:row;
align-items: center;
justify-content: flex-start;
width: 100%;
gap:2rem;
color: #6a6a6a;
padding: 0.2rem;
`;

export const AddListButton = styled(InviteButton)`
display:inline;
width: 5rem;
margin: 0.2rem;
padding: 0.2rem;
font-size: 1rem;
width: 100%;
background-color: #6248ff;
color: #fff;
`;

export const IconWrapper = styled.div`    
    display: flex;
    justify-content: right;
    width: 100%;
    cursor:pointer;

    &:hover{
        color:black;
    }
`;